<title>Toast Catering Inquiry</title>
<link href="css/package-styles.css" rel="stylesheet" type="text/css">
<?php

$my_email = "sales@charlestonhospitalitygroup.com";

/*

Enter the continue link to offer the user after the form is sent.  If you do not change this, your visitor will be given a continue link to your homepage.

If you do change it, remove the "/" symbol below and replace with the name of the page to link to, eg: "mypage.htm" or "http://www.elsewhere.com/page.htm"

*/

$continue = "http://toastofcharleston.com";

/*

Step 3:

Save this file (FormToEmail.php) and upload it together with your webpage containing the form to your webspace.  IMPORTANT - The file name is case sensitive!  You must save it exactly as it is named above!  Do not put this script in your cgi-bin directory (folder) it may not work from there.

THAT'S IT, FINISHED!

You do not need to make any changes below this line.

*/

$errors = array();

// Remove $_COOKIE elements from $_REQUEST.

if(count($_COOKIE)){foreach(array_keys($_COOKIE) as $value){unset($_REQUEST[$value]);}}

// Check all fields for an email header.

function recursive_array_check_header($element_value)
{

global $set;

if(!is_array($element_value)){if(preg_match("/(%0A|%0D|\n+|\r+)(content-type:|to:|cc:|bcc:)/i",$element_value)){$set = 1;}}
else
{

foreach($element_value as $value){if($set){break;} recursive_array_check_header($value);}

}

}

recursive_array_check_header($_REQUEST);

if($set){$errors[] = "You cannot send an email header";}

unset($set);

// Validate email field.

if(isset($_REQUEST['email']) && !empty($_REQUEST['email']))
{

if(preg_match("/(%0A|%0D|\n+|\r+|:)/i",$_REQUEST['email'])){$errors[] = "Email address may not contain a new line or a colon";}

$_REQUEST['email'] = trim($_REQUEST['email']);

if(substr_count($_REQUEST['email'],"@") != 1 || stristr($_REQUEST['email']," ")){$errors[] = "Email address is invalid";}else{$exploded_email = explode("@",$_REQUEST['email']);if(empty($exploded_email[0]) || strlen($exploded_email[0]) > 64 || empty($exploded_email[1])){$errors[] = "Email address is invalid";}else{if(substr_count($exploded_email[1],".") == 0){$errors[] = "Email address is invalid";}else{$exploded_domain = explode(".",$exploded_email[1]);if(in_array("",$exploded_domain)){$errors[] = "Email address is invalid";}else{foreach($exploded_domain as $value){if(strlen($value) > 63 || !preg_match('/^[a-z0-9-]+$/i',$value)){$errors[] = "Email address is invalid"; break;}}}}}}

}

// Check referrer is from same site.

if(!(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']) && stristr($_SERVER['HTTP_REFERER'],$_SERVER['HTTP_HOST']))){$errors[] = "You must enable referrer logging to use the form";}

// Check for a blank form.

function recursive_array_check_blank($element_value)
{

global $set;

if(!is_array($element_value)){if(!empty($element_value)){$set = 1;}}
else
{

foreach($element_value as $value){if($set){break;} recursive_array_check_blank($value);}

}

}

recursive_array_check_blank($_REQUEST);

if(!$set){$errors[] = "You cannot send a blank form";}

unset($set);

// Display any errors and exit if errors exist.

if(count($errors)){foreach($errors as $value){print "$value<br>";} exit;}

if(!defined("PHP_EOL")){define("PHP_EOL", strtoupper(substr(PHP_OS,0,3) == "WIN") ? "\r\n" : "\n");}

// Build message.

function build_message($request_input){if(!isset($message_output)){$message_output ="";}if(!is_array($request_input)){$message_output = $request_input;}else{foreach($request_input as $key => $value){if(!empty($value)){if(!is_numeric($key)){$message_output .= str_replace("_"," ",ucfirst($key)).": ".build_message($value).PHP_EOL.PHP_EOL;}else{$message_output .= build_message($value).", ";}}}}return rtrim($message_output,", ");}

$message = build_message($_REQUEST);

$message = $message . PHP_EOL.PHP_EOL."-- ".PHP_EOL."";

$message = stripslashes($message);

$subject = "Toast Catering Inquiry";

$headers = "From: " . $_REQUEST['email'];

mail($my_email,$subject,$message,$headers);

?>

<!-- include your own success html here -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>Toast of the Town</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <link rel="SHORTCUT ICON" href="favicon.ico">
    <meta name="description" content="Toast of the Town is about celebrating individuals who have overcome great challenges or obstacles.">
    <meta name="keywords" content="Toast, Toast of Charleston, Toast of the Town, Toast of Summerville, Live 5 News, Charleston Restaurant">
    <meta name="robots" content="index, follow">
    <link href="css/global.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
    <link rel="canonical" href="http://toastofcharleston.com">
    <link rel="publisher" href="https://plus.google.com/+ToastCharleston">
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Toast of the Town">
    <meta property="og:description" content="Toast of the Town is about celebrating individuals who have overcome great challenges or obstacles.">
    <meta property="og:url" content="http://toastofcharleston.com">
    <meta property="og:site_name" content="Toast!">
    <meta property="article:publisher" content="https://www.facebook.com/ToastofCharleston?ref=tn_tnmn">
    <meta itemprop="name" content="Toast!">
	<meta itemprop="description" content="Toast of the Town is about celebrating individuals who have overcome great challenges or obstacles.">
	<meta itemprop="image" content="http://toastofcharleston.com/index.html"> 
    <script type="text/javascript">
function MM_swapImgRestore() { //v3.0
    var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
    function MM_preloadImages() { //v3.0
    var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }

    function MM_findObj(n, d) { //v4.01
    var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
    if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
    for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
    if(!x && d.getElementById) x=d.getElementById(n); return x;
    }

    function MM_swapImage() { //v3.0
    var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
    if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
    </script>
    
    <style type="text/css">
	    body {
		    background-image: none;
	    }
	    
	    .tott-header {
		    max-width: 100%;
		    max-height: 100%;
		    margin: 0 auto;
		    display: block;
	    }
	    
	    .tott-logo {
		    margin: -80px auto 0 auto;
		    display: block;
		    max-width: 100%;
		    max-height: 100%;
	    }
	    
	    .tott-content {
		    text-align: center;
		    font-size: 26px;
	    }
	    
	    .tott-rewards {
		    margin: 0 auto 40px auto;
		    width: 300px;
		    font-size: 22px;
	    }
	    
	    #contact-form {
		    background-color: #600404;
		    color: #FFF;
		    padding: 25px;
	    }
	    
	    #contact-form input {
		    width: 250px;
		    padding: 5px;
		    margin: 5px 10px;
		    font-weight: bold;
		    color: #600404;
	    }
	    
	    #contact-form p {
		    font-size: 18px;
		    color: #FCE86B;
	    }
	    
	    .nominee {
		    color: #FCE86B;
		    text-shadow: 3px 3px 4px #000;
		    position: relative;
		    top: 65px;
		    z-index: 9999;
		    right: 50px;
	    }
	    
	    .form-top, .form-bottom {
		    max-width: 100%;
		    max-height: 100%;
	    }
	    
	    .form-top {
		    top: 4px;
		    position: relative;
	    }
	    
	    #contact-form .two-col {
		    display: inline-block;
		    vertical-align: top;
	    }
	    
	    .form-message {
		    height: 50px;
	    }
	    
	    .submit-button {
		    background-color: #FCE86B;
		    border: none;
		    text-transform: uppercase;
		    font-weight: bold;
		    letter-spacing: 1px;
		    font-size: 16px;
		    min-width: 265px;
	    }
	    
	    .disclaimer {
		    width: 250px;
		    font-size: 12px;
		    margin: 10px 0 0 10px;
	    }
	    
	    .spacing {
		    margin: 100px 0;
	    }
	    
    </style>
        <!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '896758270739348');

fbq('track', 'PageView');

</script>

<noscript>

<img height="1" width="1"

src="https://www.facebook.com/tr?id=896758270739348&ev=PageView

&noscript=1"/>

</noscript>

<!-- End Facebook Pixel Code -->
</head>

<body onload="MM_preloadImages('images/facebookB.png','images/twitterB.png')">
	<img src="images/tott-header.png" alt="Toast of the Town Header" class="tott-header"/>
	    <div class="container">
	    
        <div class="content">
            <img src="images/ann-logo.png" alt="Toast!" class="tott-logo" />
			<div class="spacing"></div>
           <p class="tott-content">Thank you!</p>
           <p class="tott-content">We have successfully received your inquiry and will be in touch.</p>
            <div class="spacing"></div>
           
                       <!--end content-->
        </div><!--end container-->
    </div>
    
    
    
   <div id="basement">
	<div class="pineapple-wrapper">
		<div class="pineapple"></div><!-- ./pineapple -->
	</div><!-- ./pineapple-wrapper -->
	<a href="http://charlestonhospitalitygroup.com" title="Charleston Hospitality Group" target="_blank" class="basement-link"><p>Charleston Hospitality Group</p></a>
	<div class="basement-container">
		<div class="fltlft">
	<a href="http://toastofcharleston.com" title="Toast!" target="_blank"><img src="images/logos/toast.png" alt="toast" width="80" height="80"></a>
	<a href="http://charlestonhospitalitycatering.com" title="Charleston Hospitality Catering" target="_blank"><img src="images/logos/catering.png" alt="catering" width="80" height="80"></a>
	<a href="http://elistable.com" title="Eli's Table" target="_blank"><img src="images/logos/elis.png" alt="elis" width="80" height="80"></a>
	</div> <!-- ./fltlft -->
	<div class="fltrght">
	<a href="http://queology.com" title="Queology BBQ" target="_blank"><img src="images/logos/queology.png" alt="queology" width="80" height="80"></a>
	<a href="http://tabbuli.com" title="Tabbuli" target="_blank"><img src="images/logos/tabbuli.png" alt="tabbuli" width="80" height="80"></a>
	<a href="http://honkytonksaloon.com" title="Honky Tonk Saloon" target="_blank"><img src="images/logos/hts.png" alt="Honky Tonk Saloon" width="80" height="80"></a>
		</div><!-- ./fltrt -->
	</div><!-- ./container -->
	<div class="clearfix"></div>
	
</div><!-- ./basement -->
    <div class="ecard"><a href="https://dianas.alohaenterprise.com/memberlink/GiftCards.html?companyID=dia06" title="Check E-Card Balance" target="_blank">Check E-Card Balance</a></div>
</body>
</html>
